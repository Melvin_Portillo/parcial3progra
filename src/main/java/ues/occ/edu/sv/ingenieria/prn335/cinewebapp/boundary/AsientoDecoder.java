/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.cinewebapp.boundary;

import javax.ejb.LocalBean;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbException;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.Asiento;

/**
 *
 * @author vladimir
 */
@LocalBean
public class AsientoDecoder implements Decoder.Text<Asiento> {

    Jsonb jsonb = JsonbBuilder.create();
    
    
    @Override
    public Asiento decode(String string) throws DecodeException {
        if (string != null && !string.isEmpty() && !string.equals("")) {
            try{
                return jsonb.fromJson(string,Asiento.class);
            } catch (JsonbException ex) {
                System.out.println("algo fallo");
            }
        }
        return null;
    }

    @Override
    public boolean willDecode(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void init(EndpointConfig ec) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void destroy() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
