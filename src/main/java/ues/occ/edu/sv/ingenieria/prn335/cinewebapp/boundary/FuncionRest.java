/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.cinewebapp.boundary;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.control.FuncionFacade;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.Boleto;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.Funcion;

/**
 *
 * @author vladimir
 */
@Path("boundary/funcion")
public class FuncionRest implements Serializable{
       
    @Inject
    FuncionFacade funcionFacade;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response getFuncionById(@PathParam("id") int idFuncion){
       
        Funcion resp = funcionFacade.find(idFuncion);
        
        if(resp == null){
            
            return Response.noContent().build();
            
        }
        
        return Response.ok(resp).build();
        
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/disponibles")
    public Response getBoletosByFuncion(@PathParam("id") int idFuncion){
       
        Funcion resp = funcionFacade.find(idFuncion);
        
        if(resp == null){
            
            return Response.noContent().build();
            
        }
        
        List <Boleto> boletoList = resp.getBoletoList();
        
        return Response.ok(boletoList).build();
                        
    }
    
            
    
}
