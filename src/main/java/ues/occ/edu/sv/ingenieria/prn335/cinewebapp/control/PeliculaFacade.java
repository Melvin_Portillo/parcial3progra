/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.cinewebapp.control;

import java.util.Collections;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.Pelicula;

/**
 *
 * @author vladimir
 */
@Stateless
public class PeliculaFacade extends AbstractFacade<Pelicula>{
    
     @PersistenceContext(unitName = "cinePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PeliculaFacade() {
        super(Pelicula.class);
    }

    
        
    public List<Pelicula> getPeliculasBySucursal(Integer sucursalId){
        
        Query q = em.createQuery("SELECT DISTINCT p FROM Pelicula p JOIN Funcion AS f JOIN Boleto AS b JOIN AsientoSala AS ass JOIN Sala as sa JOIN Sucursal AS su ON su.idSucursal=:idSucursal");
        q.setParameter("idSucursal", sucursalId);
        
        return q.getResultList();
        
    }
    
}
