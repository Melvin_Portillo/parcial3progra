/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.cinewebapp.boundary;

import javax.ejb.LocalBean;
import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbException;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.control.AsientoSalaFacade;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.Asiento;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.AsientoSala;

/**
 *
 * @author vladimir
 */
@LocalBean
public class AsientoEncoder implements Encoder.Text<Asiento>{

    
    Jsonb jsonb = JsonbBuilder.create();

    @Inject
    private AsientoSalaFacade facadeasientosala;
    AsientoSala asiento;
 
    public void mensajeConfirma(String mensaje){
        if (mensaje!=null && mensaje.equals("comprare boleto")) {
            asiento.setActivo(true);
            facadeasientosala.edit(asiento);
        }
    }
    
    @Override
    public String encode(Asiento t) throws EncodeException {
        if (t !=null) {
            try{
               return jsonb.toJson(t);
            } catch (JsonbException ex) {
                System.out.println("fallo ");
            }
        }
        return null;
    }

    @Override
    public void init(EndpointConfig ec) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void destroy() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public AsientoSala getAsiento() {
        return asiento;
    }

    public void setAsiento(AsientoSala asiento) {
        this.asiento = asiento;
    }
    
    
}
