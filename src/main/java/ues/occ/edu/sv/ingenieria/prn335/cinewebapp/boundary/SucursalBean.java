/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.cinewebapp.boundary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.WeakHashMap;
import javax.annotation.PostConstruct;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.swing.event.ChangeEvent;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import org.primefaces.event.SelectEvent;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.control.FuncionFacade;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.control.PeliculaFacade;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.Funcion;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.Pelicula;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.Sucursal;

/**
 *
 * @author carlos
 */
@FacesConverter("entidadConverter")
@Named(value = "sucursalBean")
@ViewScoped
public class SucursalBean implements Serializable, Converter{

    private static Map<Object, String> entities = new WeakHashMap<Object, String>();
    
    @Inject
    FuncionFacade funcionFacade;
    
    @Inject
    PeliculaFacade peliculaFacade;
    List<Sucursal> sucursales;
    Sucursal selectedSucursal;
    List<Pelicula> peliculaList;
    List<Funcion> funcionList;
    Pelicula peliculaSelected;
    Client cliente;
    WebTarget raiz;

    public SucursalBean() {
        this.cliente = ClientBuilder.newClient();
        this.raiz = cliente.target("http://localhost:8080/parcial3progra");
    }

    @PostConstruct
    public void iniciar() {
        Response respuesta = this.raiz.path("/servicios/boundary/sucursal/sucursales").request().get();
        sucursales = respuesta.readEntity(new GenericType<List<Sucursal>>() {
        });
    }

    public void onSucursalSelect() {
        Response respuesta = this.raiz.path("/servicios/boundary/sucursal/"+ selectedSucursal.getIdSucursal() +"/peliculas").request().get();
        peliculaList = respuesta.readEntity(new GenericType<List<Pelicula>>() {
        });
    }
    
    public void onPeliculaSelect(Integer idPelicula){
        peliculaSelected = peliculaFacade.find(idPelicula);
        System.out.println("PeliculaSelected: "+ idPelicula);
        funcionList = funcionFacade.getFuncionByIdPelicula(idPelicula);
    }

    public List<Sucursal> getSucursales() {
        return sucursales;
    }

    public void setSucursales(List<Sucursal> sucursales) {
        this.sucursales = sucursales;
    }

    public Sucursal getSelectedSucursal() {
        return selectedSucursal;
    }

    public void setSelectedSucursal(Sucursal selectedSucursal) {
        this.selectedSucursal = selectedSucursal;
    }

    public List<Pelicula> getPeliculaList() {
        return peliculaList;
    }

    public void setPeliculaList(List<Pelicula> peliculaList) {
        this.peliculaList = peliculaList;
    }

    public Pelicula getPeliculaSelected() {
        return peliculaSelected;
    }

    public void setPeliculaSelected(Pelicula peliculaSelected) {
        this.peliculaSelected = peliculaSelected;
    }

    public List<Funcion> getFuncionList() {
        return funcionList;
    }

    public void setFuncionList(List<Funcion> funcionList) {
        this.funcionList = funcionList;
    }
    
    

    @Override
    public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
        for (Map.Entry<Object, String> entry : entities.entrySet()) {
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object object) {
        synchronized (entities) {
            if (!entities.containsKey(object)) {
                String uuid = UUID.randomUUID().toString();
                entities.put(object, uuid);
                return uuid;
            } else {
                return entities.get(object);
            }
        }
    }

}
