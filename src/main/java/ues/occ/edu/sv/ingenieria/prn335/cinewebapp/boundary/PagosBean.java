/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.cinewebapp.boundary;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.TipoPago;

/**
 *
 * @author vladimir
 */
@Named(value = "pagosBean")
@ViewScoped
public class PagosBean implements Serializable{
    
    WebTarget webtarget;
    Client cliente;
    List<TipoPago> tipoPagosList;
    TipoPago SelectTipo;
    public PagosBean() {
    this.cliente = ClientBuilder.newClient();
    this.webtarget=cliente.target("http://localhost:8080/parcial3progra");
    }
    
    @PostConstruct
    public void iniciar(){
        Response respuesta =this.webtarget.path("/servicios/boundary/pago/tipopagos").request().get();
        tipoPagosList=respuesta.readEntity(new GenericType<List<TipoPago>>(){
        });
    }

    public TipoPago getSelectTipo() {
        return SelectTipo;
    }

    public void setSelectTipo(TipoPago SelectTipo) {
        this.SelectTipo = SelectTipo;
    }

    public List<TipoPago> getTipoPagosList() {
        return tipoPagosList;
    }

    public void setTipoPagosList(List<TipoPago> tipoPagosList) {
        this.tipoPagosList = tipoPagosList;
    }
    
    
}
