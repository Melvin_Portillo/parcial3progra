/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.cinewebapp.boundary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.control.PagoFacade;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.control.TipoPagoFacade;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.TipoPago;

/**
 *
 * @author vladimir
 */
@Path("boundary/pago")
public class PagosRest implements Serializable{
    
    @Inject
    private TipoPagoFacade tipopagofacade;
    private List<TipoPago> tipopagos=new ArrayList();
    
    public void tipoPagos(){
        tipopagos = tipopagofacade.findAll();
    }
    
    @GET
    @Path("tipopagos")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getTipoPagos(){
        tipoPagos();
        return Response.ok(this.tipopagos).build();
    }
    
    
}

