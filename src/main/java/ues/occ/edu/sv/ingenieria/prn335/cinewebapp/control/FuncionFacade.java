/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.cinewebapp.control;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.Funcion;

/**
 *
 * @author vladimir
 */
@Stateless
public class FuncionFacade extends AbstractFacade<Funcion>{
    
    @PersistenceContext(unitName = "cinePU")
    private EntityManager em;

    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FuncionFacade() {
        super(Funcion.class);
    }
    
    //SELECT DISTINCT f FROM Pelicula p JOIN Funcion AS f JOIN Boleto AS b JOIN AsientoSala AS ass JOIN Sala as sa JOIN Sucursal AS su ON su.idSucursal=:idSucursal AND Pelicula p =:idPelicula
    
    
    public List<Funcion> getFuncionBySucursal(Integer idSucursal){
        
        //"SELECT DISTINCT FROM Boleto b JOIN b.idFuncion f JOIN f.idPelicula p JOIN b.idAsiento ass JOIN ass.idSala sa JOIN sa.idSucursal su "
        
        Query q = em.createQuery("SELECT b.idFuncion FROM Boleto b JOIN b.idAsiento ass JOIN ass.idSala sa WHERE sa.idSucursal.idSucursal = :idS");
        q.setParameter("idS", idSucursal);
        
        return q.getResultList();
        
    }
    
    public List<Funcion> getFuncionByIdPelicula(Integer idPelicula){
        Query q = em.createQuery("SELECT f FROM Funcion f WHERE f.idPelicula.idPelicula = :pelicula");
        q.setParameter("pelicula", idPelicula);
        
        return q.getResultList();
    }
}
