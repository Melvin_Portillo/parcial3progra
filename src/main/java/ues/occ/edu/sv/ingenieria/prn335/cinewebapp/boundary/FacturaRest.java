/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.cinewebapp.boundary;

import com.sun.corba.se.spi.activation._ActivatorImplBase;
import java.util.List;
import javax.ejb.PostActivate;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.control.BoletoFacade;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.control.FacturaFacade;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.Boleto;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.Factura;

/**
 *
 * @author luismarioram99
 */
@Path("boundary/factura")
public class FacturaRest {
    
    @Inject
    FacturaFacade facturaFacade;
    @Inject
    BoletoFacade boletoFacade;
    
    @GET
    @Path("{id}/boletos")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBoletosByFactura(@PathParam("id") int idFactura){
        
        Factura selected = facturaFacade.find(idFactura);
        
        if(selected == null){
            
            return Response.noContent().build();
            
        }
        
        return Response.ok(selected.getBoletoList()).build();
        
    }
    
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFacturaById(@PathParam("id") int idFactura){
        
        Factura selected = facturaFacade.find(idFactura);
        
        if(selected == null){
            
            return Response.noContent().build();
            
        }
        
        return Response.ok(selected).build();
        
    }
    
    @POST
    @Path("reserva")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createFactura(List<Boleto> boletoList){
        
        Factura factura = new Factura();
        
        facturaFacade.create(factura);
        
        if(boletoList != null && !boletoList.isEmpty()){
            
            for(Boleto b: boletoList){
                b.getFacturaList().add(factura);
                boletoFacade.edit(b);
            }
            
            factura.setBoletoList(boletoList);
            
            return Response.ok("Exito").build();
            
        }
        
        return Response.noContent().build();
                
    }
    
    
    
}
