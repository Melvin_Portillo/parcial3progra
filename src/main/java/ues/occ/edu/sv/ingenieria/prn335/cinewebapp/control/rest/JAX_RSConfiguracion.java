/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.cinewebapp.control.rest;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.boundary.FacturaRest;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.boundary.FuncionRest;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.boundary.PagosRest;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.boundary.SucursalRest;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.control.rest.*;

/**
 *
 * @author vladimir
 */
@ApplicationPath("servicios")
public class JAX_RSConfiguracion extends Application{
    
    
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classe = new HashSet<>();
        addResourceClasses(classe);
        return classe;
    }

    private void addResourceClasses(Set<Class<?>> classe) {
       classe.add(SucursalRest.class);
       classe.add(FuncionRest.class);
       classe.add(FacturaRest.class);
        classe.add(PagosRest.class);
       classe.add(PeliculaFacadeREST.class);
       classe.add(AsientoFacadeREST.class);
       classe.add(AsientoSalaFacadeREST.class);
       classe.add(AtributoAsientoFacadeREST.class);
       classe.add(AtributoFuncionFacadeREST.class);
       classe.add(AtributoSalaFacadeREST.class);
       classe.add(BoletoFacadeREST.class);
       classe.add(CaracteristicaAsientoFacadeREST.class);
       classe.add(CaracteristicaFuncionFacadeREST.class);
       classe.add(CaracteristicaSalaFacadeREST.class);
       classe.add(ClasificacionFacadeREST.class);
       classe.add(DescuentoFacadeREST.class);
       classe.add(DirectorFacadeREST.class);
       classe.add(FacturaFacadeREST.class);
       classe.add(FuncionFacadeREST.class);
       classe.add(GeneroFacadeREST.class);
       classe.add(MenuConsumibleFacadeREST.class);
       classe.add(OrdenConsumibleFacadeREST.class);
       classe.add(OrdenFacadeREST.class);
       classe.add(PagoFacadeREST.class);
       classe.add(PeliculaFacadeREST.class);
       classe.add(SalaFacadeREST.class);
       classe.add(SucursalFacadeREST.class);
       classe.add(TipoDescuentoFacadeREST.class);
       classe.add(TipoPagoFacadeREST.class);
       
    }
    
}
