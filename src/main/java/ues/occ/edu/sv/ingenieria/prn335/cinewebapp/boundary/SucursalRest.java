/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.cinewebapp.boundary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.Query;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.control.PeliculaFacade;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.control.SucursalFacade;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.Pelicula;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.entity.Sucursal;

/**
 *
 * @author vladimir
 */
@Path("boundary/sucursal")
public class SucursalRest implements Serializable{
    
    @Inject
    private PeliculaFacade peliculafacade;
    private List<Pelicula> peli=new ArrayList();
    @Inject
    private SucursalFacade facade;
    private List<Sucursal> sucursal=new ArrayList();
    
    @GET
    @Path("{id}/peliculas")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getPeliculas(@PathParam("id") Integer idSucursal){
        
        List<Pelicula> peliculas = this.peliculafacade.getPeliculasBySucursal(idSucursal);
        
        return Response.ok(peliculas).build();
    }
    
    public void sucursales(){
        sucursal = facade.findAll();
    }
    
    @GET
    @Path("sucursales")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getSucursales(){
     sucursales();
        return Response.ok(this.sucursal).build();
    }
    
}
