/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.cinewebapp.boundary;

import java.io.IOException;
import java.io.Serializable;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import javax.inject.Inject;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import ues.occ.edu.sv.ingenieria.prn335.cinewebapp.control.AsientoFacade;

/**
 *
 * @author vladimir
 */
@ServerEndpoint(value = "/server")
public class WebSocketServer implements Serializable{
    
    @Inject
    private AsientoEncoder encoderAsiento;
    @Inject
    private AsientoDecoder decoderAsiento;
    @Inject
    private AsientoFacade asientoFacade;
    
    public Session sesion;
    private static final Set<WebSocketServer> clienteEnd = new CopyOnWriteArraySet();
    
    
        @OnOpen
    public void onOpen(Session sesion) throws IOException {
        this.sesion = sesion;
        clienteEnd.add(this);
    }

    @OnMessage
    public void onMessage(String m){
        encoderAsiento.mensajeConfirma(m);
    }
    
}
